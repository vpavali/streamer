
#include <vector>
#include <string>
#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <spdlog/spdlog.h>
#include "except.h"
#include "handlers/hlsv3.h"
#include "mpegts.h"

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavfilter/avfilter.h>
#include <libavutil/avutil.h>
#include <libavutil/error.h>
}

static char errorbuf[1024];

auto logger = spdlog::stdout_logger_mt("mpegts");

static int readFunction(void* opaque, uint8_t* buf, int buf_size)
{
	auto& me = *reinterpret_cast<Buffer*>(opaque);

	auto read_sz = std::min(buf_size, (int) me.size());

	auto last = me.begin() + read_sz;
	std::copy(me.begin(), last, buf);
	me.erase(me.begin(), last);
	return read_sz;
}

static int write_function(void* opaque, uint8_t* buf, int buf_size)
{
	auto& me = *reinterpret_cast<Buffer*>(opaque);
	// printf("writing %d bytes to %lu\n", buf_size, me.size());

	me.insert(me.end(), buf, buf + buf_size);
	return buf_size;
}

class Input
{
	Buffer ibuf;
	int _index;
	uint64_t _last_dts = 0;
	bool _eof = false;

	std::shared_ptr<uint8_t> avio_ctx_buffer;
	std::shared_ptr<AVIOContext> avioContext;
	std::shared_ptr<AVFormatContext> avFormat;

	Input(const Input & o) = delete;
	Input & operator=(const Input & o) = delete;
public:

	uint64_t start_dts;
	Input(Buffer ibuf_, int index, uint64_t start_dts_):
		avio_ctx_buffer((uint8_t*)av_malloc(8192), &av_free)
	{
		start_dts = start_dts_;
		this->ibuf = ibuf_;
		_index = index;
		int ret = 0;

		avioContext = std::shared_ptr<AVIOContext>(avio_alloc_context(avio_ctx_buffer.get(), 8192, 0, reinterpret_cast<void*>(&ibuf), &readFunction, nullptr, nullptr), &av_free);

		avFormat = std::shared_ptr<AVFormatContext>(avformat_alloc_context(), &avformat_free_context);
		auto avFormatPtr = avFormat.get();
		avFormat->pb = avioContext.get();

		auto ifmt = av_find_input_format("mp4");
		ENFORCE(ifmt, "failed to find input format mp4");

		ret = avformat_open_input(&avFormatPtr, "dummyFilename", ifmt, nullptr);

		// av_dump_format(avFormatPtr, 0, 0, 0);

		ENFORCE(ret == 0, fmt::format("avformat_open_input failed: {} {}", ret, av_strerror(ret, errorbuf, sizeof(errorbuf))));
	}

	bool read_packet(AVPacket * pkt)
	{
		auto ret = av_read_frame(avFormat.get(), pkt);

		if(ret)
		{
			// fmt::print("last av_read_frame() ret: {} {}\n", ret, av_strerror(ret, errorbuf, sizeof(errorbuf)));
			_eof = true;
		}
		else
		{
			_last_dts = pkt->dts;
		}

		return ret == 0;
	}

	bool eof()
	{
		return _eof;
	}

	uint64_t last_dts()
	{
		return _last_dts;
	}

	int index() const
	{
		return _index;
	}

	AVStream * stream()
	{
		return avFormat->streams[0];
	}
};

typedef std::shared_ptr<Input> InputPtr;

class Output
{
	Buffer & obuf;
	std::shared_ptr<uint8_t> avio_ctx_buffer;
	std::shared_ptr<AVIOContext> avioContext;
	std::shared_ptr<AVFormatContext> avFormat;

public:

	Output(Buffer & obuf_, std::vector<AVStream*> streams):
		obuf(obuf_),
		avio_ctx_buffer((uint8_t*)av_malloc(8192), &av_free)
	{
		int ret = 0;

		avioContext = std::shared_ptr<AVIOContext>(avio_alloc_context(avio_ctx_buffer.get(), 8192, 0, reinterpret_cast<void*>(&obuf), nullptr, &write_function, nullptr), &av_free);

		avFormat = std::shared_ptr<AVFormatContext>(avformat_alloc_context(), &avformat_free_context);
		auto avFormatPtr = avFormat.get();
		avFormat->pb = avioContext.get();

		auto ofmt = av_guess_format("mpegts", NULL, NULL);
		ENFORCE(ofmt, "failed to find output format mpegts");

		avFormat->oformat = ofmt;

		for(size_t i=0; i<streams.size(); ++i)
		{
			auto s = streams[i];
			auto cid = streams[i]->codec->codec_id;
			auto c = avcodec_find_encoder(cid);

			auto ns = avformat_new_stream(avFormat.get(), c);
			ENFORCE(ns != nullptr);

			ns->time_base.num = 1;
			ns->time_base.den = 90000;

			ret = avcodec_copy_context(ns->codec, s->codec);
			ENFORCE(ret == 0, fmt::format("avcodec_copy_context failed: {} {}", ret, av_strerror(ret, errorbuf, sizeof(errorbuf))));
		}

		// av_dump_format(avFormat.get(), 0, 0, 1);
		ret = avformat_write_header(avFormat.get(), NULL);
		ENFORCE(ret == 0, fmt::format("avformat_write_header failed: {} {}", ret, av_strerror(ret, errorbuf, sizeof(errorbuf))));
	}

	AVStream * stream(int idx)
	{
		assert(idx < avFormat->nb_streams);

		return avFormat->streams[idx];
	}

	void write_packet(AVPacket * pkt)
	{
		auto ret = av_interleaved_write_frame(avFormat.get(), pkt);

		ENFORCE(ret == 0, fmt::format("av_interleaved_write_frame() failed ({}): {}", ret, av_strerror(ret, errorbuf, sizeof(errorbuf))));
	}

	void flush()
	{
		auto ret = av_write_trailer(avFormat.get());

		ENFORCE(ret == 0, fmt::format("av_write_trailer() failed ({}): {}", ret, av_strerror(ret, errorbuf, sizeof(errorbuf))));
	}
};

void dump(Buffer & buf, std::string f)
{
	auto fp = fopen(f.c_str(), "wb");

	auto sz = fwrite(buf.data(), 1, buf.size(), fp);

	ENFORCE(sz == buf.size());

	fclose(fp);

}

bool select_input(InputPtr & i, std::vector<InputPtr> & inputs)
{
	auto min_dts = INT64_MAX;

	i = nullptr;

	for(auto &x: inputs)
	{
		if(!x->eof() && x->last_dts() < min_dts)
		{
			min_dts = x->last_dts();
			i = x;
		}
	}

	return i != nullptr;
}

Buffer ts_multiplex(const std::vector<Hlsv3Handler::SegmentInfo> & es_list)
{
	Buffer obuf;

	std::vector<InputPtr> inputs;

	for(auto es: es_list)
	{
		auto i = std::make_shared<Input>(es.blob, es.track_id, es.start_dts);
		inputs.emplace_back(i);
	}

	std::vector<AVStream*> streams;

	for(auto i: inputs)
	{
		streams.push_back(i->stream());
	}

	Output output(obuf, streams);

	AVPacket pkt;
	av_init_packet(&pkt);

	// fmt::print_colored(fmt::GREEN, "reading frames...\n");

	auto bsfc = av_bitstream_filter_init("h264_mp4toannexb");

	InputPtr input;
	while(select_input(input, inputs))
	{
		// fmt::print("selected input #{}\n", input->index());
		auto r = input->read_packet(&pkt);
		if(!r)
		{
			logger->debug("input {} exhausted", input->index());
			continue;
		}

		// fmt::print("read frame: dts: {} pts: {} diff: {}\n", pkt.dts, pkt.pts, pkt.pts - pkt.dts);

		auto ist = input->stream();
		auto ost = output.stream(input->index());

		if(ist->codec->codec_id == AV_CODEC_ID_H264)
		{
			AVPacket new_pkt = pkt;

	        int a = av_bitstream_filter_filter(bsfc, ost->codec, NULL,
	                                           &new_pkt.data, &new_pkt.size,
	                                           pkt.data, pkt.size,
	                                           pkt.flags & AV_PKT_FLAG_KEY);
	        if (a > 0) {
	            av_free_packet(&pkt);
	            new_pkt.buf = av_buffer_create(new_pkt.data, new_pkt.size,
	                                           av_buffer_default_free, NULL, 0);
	            if (!new_pkt.buf)
	                abort();
	        } else if (a < 0) {
	        	throw std::runtime_error("failed to filter");
	        }
	        pkt = new_pkt;
		}

		pkt.dts += input->start_dts;
		pkt.pts += input->start_dts;

		av_packet_rescale_ts(&pkt, ist->time_base, ost->time_base);

		pkt.stream_index = input->index();

		output.write_packet(&pkt);
	}

	output.flush();

	logger->debug("muxed {} bytes", obuf.size());

	return std::move(obuf);
}
