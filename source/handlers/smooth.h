
#pragma once

#include <memory>
#include <boost/regex.hpp>
#include <spdlog/spdlog.h>
#include <mg_utils.h>
#include <storage.h>

class SmoothHandler
{
	class UrlInfo;

	const static boost::regex URL_REGEX;
	BlobStoragePtr _blob_storage;
	MetaStoragePtr _meta_storage;
	std::shared_ptr<spdlog::logger> _log;

	bool parse_request(std::string uri, UrlInfo & ui);
	void insert_live_boxes(mongo::BSONObj meta, Blob & blob, bool headers_only);
public:

	SmoothHandler(MetaStoragePtr meta_storage, BlobStoragePtr blob_storage);

	int handle(struct mg_connection * conn, boost::smatch);

};
