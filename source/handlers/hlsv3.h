
#pragma once

#include <boost/regex.hpp>
#include <mg_utils.h>
#include <storage.h>

class Hlsv3Handler
{
public:
	struct SegmentInfo;

private:

	class UrlInfo;

	const static boost::regex URL_REGEX;
	BlobStoragePtr _blob_storage;
	MetaStoragePtr _meta_storage;
	std::shared_ptr<spdlog::logger> _log;

	bool parse_request(std::string uri, UrlInfo & ui);
	bool read_es(SegmentInfo & s);
	Blob multiplex(const std::vector<SegmentInfo> & seg_list);
public:

	Hlsv3Handler(MetaStoragePtr meta_storage, BlobStoragePtr blob_storage);

	int handle(struct mg_connection * conn, boost::smatch);
};

struct Hlsv3Handler::SegmentInfo
{
	std::vector<std::string> sid_list;
	uint32_t track_id;
	uint64_t start_dts;
	Blob blob;

	std::string str()
	{
		std::string text = "";
		for(size_t i =0; i<sid_list.size(); ++i)
		{
			text += sid_list[i];
			if(i+1 != sid_list.size())
			{
				text += ",";
			}
		}
		return fmt::format("{}:{}:{}", track_id, text, start_dts);
	}
};
