
#pragma once

#include <mongoose.h>
#include <cstdarg>
#include <cstring>
#include <string>

inline void send_header(struct mg_connection * conn, const char *name, const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	char buf[4096];

	vsprintf(buf, fmt, ap);
	va_end(ap);

	mg_send_header(conn, name, buf);
}

inline void send_full_response(struct mg_connection * conn, const uint8_t * content, size_t length)
{
	// send_header(conn, "Content-Length", "%u", length);
	conn->response_len = length;
	mg_send_data(conn, content, length);
}

inline void send_full_response(struct mg_connection * conn, const char * text)
{
	send_full_response(conn, (const uint8_t*) text, strlen(text));
}
