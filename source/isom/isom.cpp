
#include <except.h>
#include <bento4/Ap4.h>
#include <bento4/Ap4TfxdAtom.h>
#include <bento4/Ap4TfrfAtom.h>
#include "isom.h"

void isom_insert_boxes(Blob & blob, uint64_t frag_time, uint64_t frag_duration, uint64_t next_frag_time, uint64_t next_frag_duration, bool headers_only)
{
	int result = 0;

	auto input_stream = new AP4_MemoryByteStream(blob.data(), blob.size());

	auto & factory = AP4_DefaultAtomFactory::Instance;
	AP4_Atom * atom = 0;
	result = factory.CreateAtomFromStream(*input_stream, atom);
	ENFORCE(!result);

	auto moof = AP4_DYNAMIC_CAST(AP4_ContainerAtom, atom);
	ENFORCE(moof != 0);
	ENFORCE(moof->GetType() == AP4_ATOM_TYPE_MOOF);

	auto tfhd = AP4_DYNAMIC_CAST(AP4_TfhdAtom, moof->FindChild("traf/tfhd"));
	ENFORCE(tfhd != nullptr);

	ENFORCE((tfhd->GetFlags() & 0x000001) == 0);

	auto traf = AP4_DYNAMIC_CAST(AP4_ContainerAtom, moof->FindChild("traf"));
	auto trun = AP4_DYNAMIC_CAST(AP4_TrunAtom, moof->FindChild("traf/trun"));

	auto tfxd = new AP4_TfxdAtom(frag_time, frag_duration);

	auto tfrf = new AP4_TfrfAtom();
	auto e = AP4_TfrfAtom::Entry();
	e.FragmentAbsoluteTime = next_frag_time;
	e.FragmentDuration = next_frag_duration;
	tfrf->AddEntry(e);

	traf->AddChild(tfxd);
	traf->AddChild(tfrf);

	trun->SetDataOffset(AP4_ATOM_HEADER_SIZE + moof->GetSize());

	Blob out;

	AP4_DataBuffer buf;
	auto bs = new AP4_MemoryByteStream(buf);

	result = moof->Write(*bs);
	ENFORCE(AP4_SUCCEEDED(result));

	out.assign(bs->GetData(), bs->GetData() + bs->GetDataSize());

    AP4_Position pos;
    result = input_stream->Tell(pos);

    ENFORCE(!result);
	ENFORCE(input_stream->GetDataSize() >= pos + 8);
	ENFORCE(memcmp(input_stream->GetData() + pos + 4, "mdat", 4) == 0);

	if(!headers_only)
	{
    	out.insert(out.end(), input_stream->GetData() + pos, input_stream->GetData() + input_stream->GetDataSize());
    }

    std::swap(blob, out);

	AP4_RELEASE(bs);
	AP4_RELEASE(input_stream);
}
