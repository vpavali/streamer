
#pragma once

#include <storage.h>

void isom_insert_boxes(Blob & blob, uint64_t frag_time, uint64_t frag_duration, uint64_t next_frag_time, uint64_t next_frag_duration, bool headers_only);
